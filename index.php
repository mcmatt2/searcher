<?php
spl_autoload_register(function($class) {
    $class = realpath(__DIR__."/src/{$class}.php");
    if ( $class ) {
        require $class;
    }
});

if ( $_GET['provider'] ) {
    require 'ajax.php';
} else {
    require 'form.php';
}
/*else if ( $_GET['q'] ) {
    require 'index.results.php';
} else {
    require 'index.landing.php';
}
*/
