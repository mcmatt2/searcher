<?php

use Interfaces\IProduct;
use Interfaces\IProvider;
use Interfaces\IStore;

class Store implements IStore {
    /**
     * @var IProvider[]
     */
    private $providers;

    public function __construct() {
        $this->providers = [];
    }

#region IStore
    /**
     * @param IProvider $provider
     *
     * @return IStore
     */
    public function addProvider(IProvider $provider) {
        if ( in_array($provider, $this->providers) )
            return $this;

        $this->providers[] = $provider;

        return $this;
    }

    /**
     * @param IProvider $provider
     *
     * @return IStore
     */
    public function removeProvider(IProvider $provider) {
        if ( !in_array($provider, $this->providers) )
            return $this;

        // TODO: Remove.

        return $this;
    }

    /**
     * @param string $query
     *
     * @return IProduct[]
     */
    public function search($query) {
        $results = [];
        foreach ( $this->providers as $provider ) {
            $results = array_merge($results, $provider->search($query));
        }
        return $results;
    }
#endregion
}
