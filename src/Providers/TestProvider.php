<?php namespace Providers;

use Interfaces\IProduct;
use Interfaces\IProvider;

class TestProvider implements IProvider {
    /** @var string */
    private $name;

    public function __construct() {
        $this->name = 'Test Provider';
    }

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    /**
     * @param string $query
     *
     * @return IProduct[]
     */
    public function search($query) {
        $results = [];
        for ( $i = 1; $i < 11; $i++ ) {
            $basePrice = (rand(0, 51200) / 100);
            $discount = (rand(40, 100) / 100);
            $currentPrice = round($basePrice * $discount, 2);

            $product = (new \Product())
                ->setName("{$query} #{$i}")
                ->setURL("https://test.provider.local/product/{$i}")
                ->setCurrentBase($currentPrice)
                ->setBasePrice($basePrice)
                ->setProvider($this)
            ;
            $results[] = $product;
        }
        return $results;
    }
}
