<?php namespace Interfaces;

/**
 * @package Interfaces
 */
interface IProduct {
    /**
     * @return string
     */
    public function getName();

    /**
     * @param string $name
     *
     * @return IProduct
     */
    public function setName($name);



    /**
     * @return string
     */
    public function getURL();

    /**
     * @param string $url
     *
     * @return IProduct
     */
    public function setURL($url);



    /**
     * @return double
     */
    public function getCurrentPrice();

    /**
     * @param double $currentPrice
     *
     * @return IProduct
     */
    public function setCurrentBase($currentPrice);



    /**
     * @return double
     */
    public function getBasePrice();

    /**
     * @param double $basePrice
     *
     * @return IProduct
     */
    public function setBasePrice($basePrice);



    /**
     * @return IProvider
     */
    public function getProvider();

    /**
     * @param IProvider $provider
     *
     * @return IProduct
     */
    public function setProvider(IProvider $provider);
}
