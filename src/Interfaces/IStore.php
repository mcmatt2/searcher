<?php namespace Interfaces;

/**
 * @package Interfaces
 */
interface IStore {
    /**
     * @param IProvider $provider
     *
     * @return IStore
     */
    public function addProvider(IProvider $provider);

    /**
     * @param IProvider $provider
     *
     * @return IStore
     */
    public function removeProvider(IProvider $provider);

    /**
     * @param string $query
     *
     * @return IProduct[]
     */
    public function search($query);
}
