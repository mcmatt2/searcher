<?php namespace Interfaces;

/**
 * @package Interfaces
 */
interface IProvider {
    /**
     * @return string
     */
    public function getName();

    /**
     * @param string $name
     *
     * @return IProvider
     */
    public function setName($name);

    /**
     * @param string $query
     *
     * @return IProduct[]
     */
    public function search($query);
}
