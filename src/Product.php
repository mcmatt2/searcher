<?php

use Interfaces\IProduct;
use Interfaces\IProvider;

class Product implements IProduct {
    /** @var string */
    private $name, $url;

    /** @var double */
    private $currentPrice, $basePrice;

    /** @var IProvider */
    private $provider;

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    public function getURL() {
        return $this->url;
    }

    public function setURL($url) {
        $this->url = $url;
        return $this;
    }

    public function getCurrentPrice() {
        return $this->currentPrice;
    }

    public function setCurrentBase($currentPrice) {
        $this->currentPrice = $currentPrice;
        return $this;
    }

    public function getBasePrice() {
        return $this->basePrice;
    }

    public function setBasePrice($basePrice) {
        $this->basePrice = $basePrice;
        return $this;
    }

    public function getProvider() {
        return $this->provider;
    }
    public function setProvider(IProvider $provider) {
        $this->provider = $provider;
        return $this;
    }
}
