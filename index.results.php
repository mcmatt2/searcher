<?php
$store = (new Store())
    ->addProvider(new \Providers\TestProvider());

$results = $store->search($_GET['q']);

usort($results, function($a, $b) {
    /**
     * @var \Interfaces\IProduct $a
     * @var \Interfaces\IProduct $b;
     */

    return $a->getCurrentPrice() > $b->getCurrentPrice();
});
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf8">
    <style>
        *, *:before, *:after { -moz-box-sizing: border-box; -webkit-box-sizing: border-box; box-sizing: border-box; }
        * { margin: 0; padding: 0; }

        body {
            font: 80% sans-serif;
        }

        h1, table { margin: 16px; }

        table { border-spacing: 0; }
        thead {
            background: #eee;
        }
        td, th { padding: 4px; }
        th { text-align: left; }

        ins { color: #090; text-decoration: none; }
        del { color: #c00; }
    </style>
</head>
<body>
<h1>Results for '<?=$_GET['q']?>'</h1>
<table>
    <thead>
        <tr>
            <th>Store</th>
            <th>Name</th>
            <th>Price</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ( $results as $result ): ?>
        <tr>
            <td><?=$result->getProvider()->getName()?></td>
            <td><?=$result->getName()?></td>
            <td>
<?php if ( $result->getCurrentPrice() === $result->getBasePrice() ): ?>
                &pound;<?=number_format($result->getBasePrice(), 2)?>
<?php else: ?>
                <ins>&pound;<?=number_format($result->getCurrentPrice(), 2)?></ins>
                <del>&pound;<?=number_format($result->getBasePrice(), 2)?></del>
<?php endif; ?>
            </td>
        </tr>
<?php endforeach; ?>
    </tbody>
</table>
</body>
</html>
