<!DOCTYPE html>
<html>
<head>
    <meta charset="utf8">
    <style>
        *, *:before, *:after { -moz-box-sizing: border-box; -webkit-box-sizing: border-box; box-sizing: border-box; }
        * { margin: 0; padding: 0; }

        body {
            font: 80% sans-serif;
        }

        form, h1, table { margin: 16px; }

        table { border-spacing: 0; }
        thead {
            background: #eee;
        }
        td, th { padding: 4px; }
        th { text-align: left; }

        ins { color: #090; text-decoration: none; }
        del { color: #c00; }
    </style>
</head>
<body>
<form>
    <p>
        <label>
            Search:
            <input name="q" type="search">
        </label>
    </p>
    <p>
        <input type="submit" value="Search">
    </p>
</form>

<h1>Results for '<?=$_GET['q']?>'</h1>
<table>
    <thead>
    <tr>
        <th>Store</th>
        <th>Name</th>
        <th>Price</th>
    </tr>
    </thead>
    <tbody>
    </tbody>
</table>
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<script src="js/handlebars.js"></script>
<script type="text/x-handlebars-template" id="tplRow">
    <tr>
        <td>{{provider}}</td>
        <td>{{name}}</td>
        <td>
            {{#if discount}}
                <ins>&pound;{{currentPrice}}</ins>
                <del>&pound;{{basePrice}}</del>
            {{else}}
                &pound;{{currentPrice}}
            {{/if}}
        </td>
    </tr>
</script>
<script>
    var search = function(q, onSuccess, onError) {
        $.getJSON('testprovider/' + q + '.json', function(results) {
            var source = $('#tplRow').html(),
                template = Handlebars.compile(source);

            var rows = '';
            results.forEach(function(result) {
                var product = {
                    provider: 'TestProvider',
                    name: result.name,
                    discount: ( result.basePrice !== result.currentPrice ),
                    basePrice: Number(result.basePrice).toFixed(2).toLocaleString(),
                    currentPrice: Number(result.currentPrice).toFixed(2).toLocaleString()
                };
                rows += template(product);
            });
            $('tbody').empty().html(rows);
            onSuccess();
        });
    };
    $(document).ready(function() {
        $('form').submit(function(e) {
            e.preventDefault();
            var form = $(this);
            var q = $(this).find('input').first().val();
            form.prop('disabled', true);
            search(q, function() {
                form.prop('disabled', false);
            }, function() {
                form.prop('disabled', false);
            });
        })
    });
</script>
</body>
</html>
