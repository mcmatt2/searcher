<?php
header('content-type: application/json;charset=utf8');

$error = NULL;

$query = $_GET['q'];
if ( !$query ) {
    $error = 'No search query specified.';
}

$providerName = $_GET['provider'];
$class = "Providers\\{$providerName}";
if ( !class_exists($class) ) {
    $error = "Unknown provider: {$providerName}.";
}

if ( $error ) {
    http_response_code(404);
    die(json_encode([ 'error' => $error ]));
}

/** @var \Interfaces\IProvider $provider */
$provider = new $class;
$results = $provider->search($query);

$json = [];

foreach ( $results as $result ) {
    $json[] = [
        'name' => $result->getName(),
        'url' => $result->getURL(),
        'basePrice' => $result->getBasePrice(),
        'currentPrice' => $result->getCurrentPrice()
    ];
}

echo json_encode($json);
